/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Miso
 */
public class OXProgramUnitTest {
    
    public OXProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    //checkNonPlay
    @Test
    public void testCheckWinNonPlay_O() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }
      public void testCheckWinNonPlay_X() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

//Horizontal_o
    @Test
    public void testCheckWinHorizontal1_O() {
        String[][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinHorizontal2_O() {
        String[][] table = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinHorizontal3_O() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
 //Horizontal_X
    @Test
    public void testCheckWinHorizontal1_X() {
        String[][] table = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinHorizontal2_X() {
        String[][] table = {{"-", "-", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinHorizontal3_X() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    //vertical_O
    @Test
    public void testCheckWinVertical1_O() {
        String[][] table = {{"O", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinVertical2_O() {
        String[][] table = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinVertical3_O() {
        String[][] table = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    //vertica_X
    @Test
    public void testCheckWinVertical1_X(){
        String[][] table = {{"X", "-", "-"}, {"X", "-", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinVertical2_X(){
        String[][] table = {{"-", "X", "-"}, {"-", "X", "-"}, {"-", "X", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinVertical3_X(){
        String[][] table = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    //Diagonal
    @Test
    public void testCheckWinDiagonal1() {
        String[][] table = {{"X", "O", "O"}, {"O", "X", "O"}, {"O", "O", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

        @Test
    public void testCheckWinDiagonal2() {
        String[][] table = {{"X", "X", "O"}, {"X", "O", "X"}, {"O", "X", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinDiagonal3() {
        String[][] table = {{"O", "X", "X"}, {"X", "O", "X"}, {"X", "X", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    public void testCheckWinDiagonal4() {
        String[][] table = {{"O", "O", "X"}, {"O", "X", "O"}, {"X", "O", "O"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    //Draw
     @Test 
    public void testCheckDraw() {
        String[][] table = {{"X", "O", "X"}, {"O", "O", "X"}, {"X", "X", "O"}};
        assertEquals(true, OXProgram.checkDraw(table));
    }



}


